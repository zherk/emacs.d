;;; init.el --- My custom setup
;;; Code:

(custom-set-variables
 '(gnutls-algorithm-priority "normal:-vers-tls1.3"))

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))


;;; Commentary:
;;

(require 'package)
(add-to-list 'load-path "~/.emacs.d/lisp/")
(require 'python)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(setq package-archives
      '(("gnu"     . "https://elpa.gnu.org/packages/")
        ("melpa-stable" . "https://stable.melpa.org/packages/")
        ("melpa"        . "https://melpa.org/packages/"))
      package-archive-priorities
      '(("melpa-stable" . 10)
        ("gnu"     . 5)
        ("melpa"        . 0)))

(require 'use-package)
;; Automatically :ensure each use-package.
(setq use-package-always-ensure t)
;; Default value for :pin in each use-package.
;; (setq use-package-always-pin "melpa-stable")


(use-package flycheck
  :pin melpa
  :init (global-flycheck-mode)
  :hook (flycheck-mode . flycheck-yamllint-setup)
  :hook (flycheck-error-list-mode . visual-line-mode))

(use-package flycheck-yamllint
  :ensure t)

(use-package flycheck-golangci-lint
  :ensure t)

(use-package async
  :pin gnu)

(use-package auto-package-update
  :ensure t
  :config
  (setq auto-package-update-delete-old-versions t)
  (auto-package-update-maybe))

(use-package solarized-theme
  :ensure t)

(use-package py-isort
  :ensure t)

(use-package magit
  :ensure t
  :pin melpa-stable
  :hook (magit-post-refresh-hook . diff-hl-magit-post-refresh)
  :bind ("C-x M-g" . magit-dispatch-popup))

(use-package virtualenvwrapper
  :ensure t)

(use-package magit-popup
  :pin melpa-stable
  :ensure t)

(use-package counsel
  :ensure t
  :config
  (counsel-mode 1)
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t)
  (setq ivy-count-format "(%d/%d) ")
  :bind (("C-c g" . counsel-git)
	     ("C-c j" . counsel-git-grep)
	     ("C-c k" . counsel-rg)
	     ("C-c l" . counsel-locate)
         ("C-s" . swiper-isearch)
         ("C-x C-f" . counsel-find-file)
         ("C-c i r" . ivy-resume)))

(use-package ag
  :ensure t)

(use-package projectile
  :ensure t
  :init (setq projectile-completion-system 'ivy)
  :init (projectile-mode)
  :init (setq projectile-switch-project-action 'venv-projectile-auto-workon)
  :bind (:map projectile-mode-map
              ("s-p" . projectile-command-map)))

(use-package counsel-projectile
  :ensure t
  :config (counsel-projectile-mode 1))

(use-package yasnippet
  :ensure t
  :init
  (yas-global-mode 1))

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown")
  :hook  (markdown-mode . flyspell-mode)
  :hook (markdown-mode . auto-fill-mode))

(use-package diff-hl
  :ensure t
  :init
  (global-diff-hl-mode))

(use-package diff-hl
  :ensure t
  :init
  (global-diff-hl-mode))

(use-package web-mode
  :ensure t
  :mode ("\\.handlebars\\'" . web-mode))

(use-package yaml-mode
  :ensure t
  :hook (yaml-mode . flycheck-mode))


(use-package dockerfile-mode
  :ensure t)

(use-package indent-tools
  :pin melpa
  :bind ("C-c >" . indent-tools-hydra/body)
  :hook (yaml-mode . indent-tools-minor-mode))

(use-package ace-window
  :ensure t
  :bind
  ("M-o" . ace-window))

(use-package python
  :mode "python-mode"
  :after (poetry)
  :config
  (setq python-indent-guess-indent-offset nil))

(use-package py-isort
  :ensure t
  :bind
  ("C-c C-i" . py-isort-buffer))

(use-package scratch
  :ensure t
  :pin melpa-stable)


(use-package json-mode
  :ensure t
  :pin melpa-stable
  :mode ("\\.avro\\'" . json-mode))

(use-package org
  :hook (org-mode . auto-fill-mode)
  :hook (org-mode . flyspell-mode))

(use-package k8s-mode
 :ensure t
 :config
 (setq k8s-search-documentation-browser-function 'browse-url-firefox)
 :hook (k8s-mode . yas-minor-mode))

(use-package  python-docstring
  :ensure t
  :hook (python-mode . python-docstring-mode))

(use-package protobuf-mode
  :ensure t
  :hook (protobuf-mode . flycheck-mode))

(use-package terraform-mode
  :ensure t
  :hook (terraform-mode . (lambda ()
                            (add-hook 'before-save-hook
                                      #'terraform-format-buffer
                                      nil t))))

(use-package rust-mode
  :ensure t)

(use-package rustic
  :ensure t)

(use-package poly-markdown
  :ensure t)

(use-package string-inflection
  :ensure t
  :bind ("C-c C" . string-inflection-camelcase)
  :bind ("C-c L" . 'string-inflection-lower-camelcase)
  :bind ("C-c C-u" . 'string-inflection-underscore))

(use-package sqlformat
  :ensure t
  :config
  (setq sqlformat-args '("--dialect" "postgres"))
  :config
  (setq sqlformat-command 'sqlfluff)
  :bind ("C-c C-f" . 'sqlformat))

(use-package multiple-cursors
  :straight t
  :ensure   t
  :bind (("H-SPC" . set-rectangular-region-anchor)
         ("C-M-SPC" . set-rectangular-region-anchor)
         ("C->" . mc/mark-next-like-this)
         ("C-<" . mc/mark-previous-like-this)
         ("C-c C->" . mc/mark-all-like-this)
         ("C-c C-SPC" . mc/edit-lines)
         ))

(setq custom-file "~/.emacs.d/customs.el")
(load custom-file)

(setq python-check-command "flake8")
(tool-bar-mode -1)
(scroll-bar-mode -1)
(delete-selection-mode 1)

;; Reload changed files
(global-auto-revert-mode t)
;; Spaces instead of tabs, always
(setq-default indent-tabs-mode nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(defun show-file-name ()
 "Show the full path file name in the minibuffer."
 (interactive)
 (message (buffer-file-name)))

(use-package apib-mode
:mode ("\\.apib\\'" . apib-mode))

(require 'desktop)
(desktop-save-mode 0)
(setq tab-width 4)

(global-set-key (kbd "C-c x") 'backward-char)
(global-set-key (kbd "C-c c") 'hs-toggle-hiding)

(use-package poly-markdown
  :ensure t)

(use-package go-mode
  :mode "\\go.mod\\'"
  :hook (go-mode . (lambda ()
                     (add-hook 'before-save-hook
                               #'gofmt-before-save
                               nil t))))


(use-package poetry
  :straight t)

(use-package pyvenv
  :straight t
  :hook (after-init . pyvenv-mode))

(use-package lsp-pyright
  :ensure t
  :custom (lsp-pyright-langserver-command "basedpyright")
  :hook (python-mode . (lambda ()
                          (require 'lsp-pyright)
                          (lsp)))
  :hook (python-ts-mode . (lambda ()
                          (require 'lsp-pyright)
                          (lsp))))

(use-package blacken
  :ensure t
  :hook (python-mode . blacken-mode)
  :hook (python-ts-mode . blacken-mode))

(use-package adoc-mode)

(flycheck-define-checker golangci-lint-custom
  "Go Lang CI Lint using XML."
  :command ("golangci-lint" "run" ".")
  :error-parser flycheck-parse-checkstyle
  :modes (go-mode))

(require 'poly-markdown)

(use-package lsp-mode
  :ensure t
  :pin melpa
  :init
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (setq lsp-keymap-prefix "C-c l")
  :hook (((typescript-ts-mode go-mode js-ts-mode js-mode python-ts-mode rust-ts-mode terraform-mode) . lsp)
         (lsp-mode . lsp-lens-mode))
  :commands lsp
  :config
  (setq gc-cons-threshold 100000000))

(use-package lsp-ui
  :config
  (setq lsp-ui-sideline-show-diagnostics t
        lsp-ui-sideline-show-hover nil
        lsp-ui-sideline-show-code-actions t
        lsp-ui-sideline-delay 2))

(use-package jest-test-mode
  :ensure t
  :commands jest-test-mode
  :hook (typescript-ts-mode js-mode))

(use-package prettier
  :pin melpa
  :hook (typescript-mode . prettier-mode)
  :hook (typescript-ts-mode . prettier-mode))

;; Key bindings
(global-set-key (kbd "C-c C-n") 'flycheck-next-error)

(defun gh/disable-all-themes ()
  "Disable all currently active themes."
  (interactive)
  (mapc #'disable-theme custom-enabled-themes))

(global-set-key (kbd "C-c t d") (lambda() (interactive) (gh/disable-all-themes) (load-theme 'solarized-dark)))
(global-set-key (kbd "C-c t l") (lambda() (interactive) (gh/disable-all-themes) (load-theme 'adwaita)))

;; Use company-capf as a completion provider.
(use-package company
  :init (global-company-mode)
  :config
  (setq company-minimum-prefix-length 2
        company-idle-delay 1))

;; Use the Debug Adapter Protocol for running tests and debugging
(use-package posframe)
(use-package dap-mode
  :hook
  (lsp-mode . dap-mode)
  (lsp-mode . dap-ui-mode))

(add-hook 'before-save-hook
          'delete-trailing-whitespace)

(define-hostmode poly-yaml-hostmode
  :mode 'yaml-mode)

(define-innermode poly-yaml-helm-chart-innermode
  :mode 'text-mode
  :head-matcher "{{"
  :tail-matcher "}}"
  :head-mode 'host
  :tail-mode 'host)

(define-polymode poly-helm-chart-mode
  :hostmode 'poly-yaml-hostmode
  :innermodes '(poly-yaml-helm-chart-innermode))

(add-to-list 'auto-mode-alist '("\\.yaml" . poly-helm-chart-mode))

(define-hostmode poly-markdown-hostmode
  :mode 'markdown-mode)

(define-innermode poly-yaml-markdown-innermode
  :mode 'yaml-mode
  :head-matcher "```yaml"
  :tail-matcher "```"
  :head-mode 'host
  :tail-mode 'host)

(define-polymode poly-markdown-mode
  :hostmode 'poly-markdown-hostmode
  :innermodes '(poly-yaml-markdown-innermode))

(add-to-list 'auto-mode-alist '("\\.md" . poly-markdown-mode))

(add-to-list 'image-types 'svg)

(setq read-process-output-max (* 4 1024 1024)) ;; 4mb

(when (eq system-type 'darwin)
  (set-face-attribute 'default nil :font "Menlo" :height 170)
  (setq mac-command-modifier 'meta)
  (setq mac-option-modifier 'super) ; make opt key do Super
)
;; snyk-secret is on ~/.emacs.d/lisp/
(add-to-list 'load-path "~/.emacs.d/lisp/")
(require 'snyk-secret)

(with-eval-after-load 'lsp-mode
  (lsp-register-client
   (make-lsp-client
    :server-id 'snyk-ls
    ;; The "-o" option specifies the issue format, I prefer markdown over HTML
    :new-connection (lsp-stdio-connection '("/usr/local/bin/snyk-ls" "-o" "md"))

    ;; Change this to the modes you want this in; you may want to include the
    ;; treesitter versions if you're using them
    :major-modes '(python-mode python-ts-mode typescript-mode typescript-ts-mode json-mode json-ts-mode terraform-mode)
    ;; Allow running in parallel with other servers. This is why Eglot isn't an

    :add-on? t

    :initialization-options
    `(
      :activateSnykCodeQuality "true"
                               :activateSnykCode "true"
                               :automaticAuthentication "true"
                               :manageBinariesAutomatically "true"
                               :integrationName "Emacs"
                               :integrationVersion ,emacs-version
                               :token ,snyk-api-key
                               :cliPath "/usr/local/bin/snyk"
                               :enableTrustedFoldersFeature "false"
                               :organization ,snyk-organization
                               )
    )))

(setq treesit-language-source-alist
      '((js "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
        (json "https://github.com/tree-sitter/tree-sitter-json")
        (python "https://github.com/tree-sitter/tree-sitter-python")
        (rust "https://github.com/tree-sitter/tree-sitter-rust")
        (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
        (yaml "https://github.com/ikatyang/tree-sitter-yaml")
        (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")))

(defun treesit-install-if-needed (mode)
  (unless (treesit-language-available-p mode)
    (treesit-install-language-grammar mode)))

(mapc #'treesit-install-if-needed [typescript javascript tsx json python yaml rust])

(setq major-mode-remap-alist
 '((typescript-mode . typescript-ts-mode)
   (javascript-mode . js-ts-mode)
   (json-mode . json-ts-mode)
   (python-mode . python-ts-mode)
   (rustic-mode . rust-ts-mode)))

(add-hook 'rust-ts-mode-hook
          (lambda () (add-hook 'before-save-hook 'rust-format-buffer nil 'local)))

;; Use official Terraform language server
(setq lsp-disabled-clients '(tfls))
;; (setq lsp-pylsp-plugins-black-enabled t)

(add-hook 'prog-mode-hook 'column-number-mode)
(add-hook 'prog-mode-hook 'hl-line-mode)
(add-hook 'prog-mode-hook 'flyspell-prog-mode)

;; Solves problems were polymode goes to the bottom of the file when LSP is enabled
(setq-default polymode-lsp-integration nil)

(provide 'init)
;;; init.el ends here
